package com.example.myapplication;
import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static com.google.common.truth.Truth.assertThat;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.robolectric.Shadows.shadowOf;
@Config(sdk = Build.VERSION_CODES.O)
@RunWith(RobolectricTestRunner.class)
public class MainActivityTest {
    private MainActivity activity;
    private TextToSpeech mTTs;
    private TextToSpeech.OnInitListener listener;
    private Button mButtonSpeak;
    private Button btnPhoto;
    private ImageView imageView;
    private Spinner mSpinner;
    private EditText mEditText;

    @Before
    public void setup() {
        activity = Robolectric.buildActivity(MainActivity.class).create().get();
        listener = new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
            }
        };
        mTTs = new TextToSpeech(activity, listener);
        mButtonSpeak = (Button) activity.findViewById(R.id.button_speak);
        btnPhoto = (Button) activity.findViewById(R.id.button4);
        imageView = (ImageView) activity.findViewById(R.id.imageView);
        mSpinner = (Spinner) activity.findViewById(R.id.lang_spinner);
        mEditText = (EditText) activity.findViewById(R.id.edit_text);
    }

    @Test
    public void testOpenCam() {
        Assert.assertTrue(this.btnPhoto.performClick());
        Instrumentation.ActivityResult result = new Instrumentation.ActivityResult(Activity.RESULT_OK, null);
        Assert.assertNotNull(result);
    }

    @Test
    public void onCreateTest() {
        System.out.println("onCreate started");
        activity = Robolectric.buildActivity(MainActivity.class).create().get();
        assertEquals("Application creation check", true, activity.isCreated);
        System.out.println("onCreate ended");
    }

    @Test
    public void testOnDestroy() {
        System.out.println("doSomething started");
        activity = Robolectric.buildActivity(MainActivity.class).create().get();
        activity.onDestroy();
        System.out.println("doSomething ended");
        assertEquals(true, activity.isDestroyed);
    }

    @Test
    public void shouldReturnTrueOnClickMethod() {
        Assert.assertTrue(this.mButtonSpeak.performClick());
        Instrumentation.ActivityResult result = new Instrumentation.ActivityResult(Activity.RESULT_OK, null);
        Assert.assertNotNull(result);
    }

    @Test
    public void shouldCheckOnInitMethod() {
        int status = TextToSpeech.SUCCESS;
        listener.onInit(status);
        assertEquals(TextToSpeech.SUCCESS, status);
        assertEquals("Eng", (String) mSpinner.getSelectedItem());
    }

    @Test
    public void imageViewShouldNotBeNull() throws Exception {
        assertThat(imageView).isNotNull();
        assertThat(shadowOf(imageView)).isNotNull();
    }

    @Test
    public void spinnerShouldNotBeNull() throws Exception {
        assertThat(mSpinner).isNotNull();
        assertThat(shadowOf(mSpinner)).isNotNull();
    }

    @Test
    public void testButtonsVisible() {
        assertThat(mButtonSpeak.getVisibility(), equalTo(View.VISIBLE));
        assertThat(btnPhoto.getVisibility(), equalTo(View.VISIBLE));
    }

    @Test
    public void mTTsShouldNotBeNull() throws Exception {
        assertThat(mTTs).isNotNull();
        assertThat(shadowOf(mTTs)).isNotNull();
    }

    @Test
    public void getContext_shouldReturnContext() throws Exception {
        assertThat(shadowOf(mTTs).getContext()).isEqualTo(activity);
    }

    @Test
    public void getOnInitListener_shouldReturnListener() throws Exception {
        assertThat(shadowOf(mTTs).getOnInitListener()).isEqualTo(listener);
    }

    @Test
    public void getLastSpokenText_shouldReturnSpokenText() throws Exception {
        mTTs.speak("Hello", TextToSpeech.QUEUE_FLUSH, null);
        assertThat(shadowOf(mTTs).getLastSpokenText()).isEqualTo("Hello");
    }

    @Test
    public void getLastSpokenText_shouldReturnMostRecentText() throws Exception {
        mTTs.speak("Hello", TextToSpeech.QUEUE_FLUSH, null);
        mTTs.speak("Hi", TextToSpeech.QUEUE_FLUSH, null);
        assertThat(shadowOf(mTTs).getLastSpokenText()).isEqualTo("Hi");
    }

    @Test
    public void clearLastSpokenText_shouldSetLastSpokenTextToNull() throws Exception {
        mTTs.speak("Hello", TextToSpeech.QUEUE_FLUSH, null);
        shadowOf(mTTs).clearLastSpokenText();
        assertThat(shadowOf(mTTs).getLastSpokenText()).isNull();
    }

    @Test
    public void isShutdown_shouldReturnFalseBeforeShutdown() throws Exception {
        assertThat(shadowOf(mTTs).isShutdown()).isFalse();
    }

    @Test
    public void isShutdown_shouldReturnTrueAfterShutdown() throws Exception {
        mTTs.shutdown();
        assertThat(shadowOf(mTTs).isShutdown()).isTrue();
    }

    @Test
    public void isStopped_shouldReturnTrueBeforeSpeak() throws Exception {
        assertThat(shadowOf(mTTs).isStopped()).isTrue();
    }

    @Test
    public void isStopped_shouldReturnTrueAfterStop() throws Exception {
        mTTs.stop();
        assertThat(shadowOf(mTTs).isStopped()).isTrue();
    }

    @Test
    public void isStopped_shouldReturnFalseAfterSpeak() throws Exception {
        mTTs.speak("Hello", TextToSpeech.QUEUE_FLUSH, null);
        assertThat(shadowOf(mTTs).isStopped()).isFalse();
    }

    @Test
    public void getQueueMode_shouldReturnMostRecentQueueMode() throws Exception {
        mTTs.speak("Hello", TextToSpeech.QUEUE_ADD, null);
        assertThat(shadowOf(mTTs).getQueueMode()).isEqualTo(TextToSpeech.QUEUE_ADD);
    }

    @Test
    public void exchangeShouldHandleNull() {
        assertEquals("Empty exchange method assertion error ", null, MainActivity.exchange(null));
    }

    @Test
    public void checkThatTextNotNull() {
        activity.text = mEditText.getText().toString();
        activity.speak("Lat");
        assertNotNull(activity.text);
    }

    @Test
    public void textToSpeechInitTestIfSuccess() {
        activity.textToSpeechInit(0, mSpinner, mTTs);
        assertNotNull(activity.resultOfTextToSpeechInit);
    }

    @Test
    public void exchangeShouldHandleString() {
        assertEquals("0 should return null", "nulle....", MainActivity.exchange("0", activity.wordDigits));
        assertEquals("1 should return viens", "viens....", MainActivity.exchange("1", activity.wordDigits));
        assertEquals("2 should return divi", "divi....", MainActivity.exchange("2", activity.wordDigits));
        assertEquals("3 should return trīs", "trīs....", MainActivity.exchange("3", activity.wordDigits));
        assertEquals("4 should return četri", "četri....", MainActivity.exchange("4", activity.wordDigits));
        assertEquals("5 should return pieci", "pieci....", MainActivity.exchange("5", activity.wordDigits));
        assertEquals("6 should return seši", "seši....", MainActivity.exchange("6", activity.wordDigits));
        assertEquals("7 should return septiņi", "septiņi....", MainActivity.exchange("7", activity.wordDigits));
        assertEquals("8 should return septiņi", "astoņi....", MainActivity.exchange("8", activity.wordDigits));
        assertEquals("9 should return septiņi", "deviņi....", MainActivity.exchange("9", activity.wordDigits));
        assertEquals("Empty string exchange method assertion error ", "", MainActivity.exchange("", ""));
        assertEquals("White space exchange method assertion error ", "....", MainActivity.exchange(" "));
        assertEquals("Digits exchange method assertion error ", "123abc", MainActivity.exchange("123abc", "abc"));
        assertEquals("Digits exchange method assertion error ", "a", MainActivity.exchange("a", "bc"));
        assertEquals("Digits exchange method assertion error ", "a1B3", MainActivity.exchange("a1B3", "bc", "12", ""));
    }
}