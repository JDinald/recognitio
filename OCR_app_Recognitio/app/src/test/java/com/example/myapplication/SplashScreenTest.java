package com.example.myapplication;

import android.os.Build;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import static org.junit.Assert.*;
@Config(sdk = Build.VERSION_CODES.O)
@RunWith(RobolectricTestRunner.class)
public class SplashScreenTest {

    private SplashScreen activity;


    @Test
    public void onCreateTest() {
        System.out.println("onCreate started");
        activity = Robolectric.buildActivity(SplashScreen.class).create().get();
        assertEquals("Application creation check", true, activity.isCreated);
        System.out.println("onCreate ended");

    }


}