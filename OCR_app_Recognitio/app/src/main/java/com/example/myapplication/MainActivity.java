package com.example.myapplication;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.icu.text.SimpleDateFormat;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageOptions;
import com.theartofdev.edmodo.cropper.CropImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    ImageView imageView;
    Button btnPhoto;
    Bitmap bitmap;
    Intent glInt;
    String s;
    int resultOfTextToSpeechInit;
    boolean isTrue;
    String letters[][] = {{"á", "à", "â", "ǎ", "ă", "ã", "ä", "å", "ắ", "ẩ"},
            {"ṩ", "Ṩ", "ṧ", "Ṧ", "ś", "Ś", "ṥ", "Ṥ", "ŝ", "Ŝ"},
            {"ḹ", "Ḹ", "ḽ", "Ḽ", "ḷ", "Ḷ", "Ḷḷ", "Ḷḷ", "Ḷḷ", "Ḷḷ"},
            {"ú", "Ú Ùù Ŭŭ Ûû", "Ù", "ù", "Ŭ", "û", "Ù", "ù", "ü", "Ü"},
            {"Ķķ Ḳḳ Ḵḵ", "ķ", "Ḳ", "ḳ", "Ḵ", "ḵ", "Ķ", "Ķ", "Ķ", "Ķ"},
            {"ë", "é", "è", "ê", "ě", "ẽ", "É", "Ë", "È", "Ě"},
            {"Ï", "í", "Ì", "ì", "Ĭ", "ĭ", "Î", "î", "ǐ", "ï"},
            {"Źź Ẑẑ", "Ź", "ź", "Ẑ", "ẑ", "ż", "Ż", "Ż", "Ż", "Ż"},
            {"Ǵǵ Ğğ Ĝĝ Ǧǧ Ġġ Ģģ Ḡḡ", "ǵ", "ğ", "ĝ", "ġ", "ḡ", "Ǧ", "Ĝ", "Ğ", "Ǵ"},
            {"Ć", "ć", "Ĉ", "ĉ", "Ċ", "ċ", "C", "c̄", "Ḉ", "ḉ"}};
    String coreLetters[] = {"ā", "š", "ļ", "ū", "ķ", "ē", "ī", "ž", "ģ", "č"};


    //var for tts
    private TextToSpeech mTTs;
    private EditText mEditText;
    private SeekBar mSeekBarPitch;
    private SeekBar mSeekBarSpeed;
    private Button mButtonSpeak;
    public boolean isCreated;
    public boolean isDestroyed;
    public String text;
    public static String[] wordDigits = {"nulle ", "viens ", "divi ", "trīs ", "četri ",
            "pieci ", "seši ", "septiņi ", "astoņi ", "deviņi "};

    public static String exchange(final String txt, final String... numbers) {
        if (txt != null && numbers != null) {
            String result = txt;
            for (int i = 0; i < numbers.length; ++i) {
                result = result.replace(Integer.toString(i), numbers[i]);
            }
            result = result.replace(" ", "....");
            return result;
        }
        return null;
    }

    public void textToSpeechInit(int status, Spinner mSpinner, TextToSpeech mTTs) {

        if (status == TextToSpeech.SUCCESS) {
            resultOfTextToSpeechInit = mTTs.setLanguage(Locale.UK);
            mSpinner.setSelection(0);

            if (resultOfTextToSpeechInit == TextToSpeech.LANG_MISSING_DATA
                    || resultOfTextToSpeechInit == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "Language not supported");
            } else {
                mButtonSpeak.setEnabled(true);
            }
        } else {
            Log.e("TTS", "Initialization failed");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isCreated = true;
        setContentView(R.layout.activity_main);
        glInt = new Intent(this, MainActivity.class);

        imageView = (ImageView) findViewById(R.id.imageView);

        bitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.pic);
        imageView.setImageBitmap(bitmap);

        btnPhoto = findViewById(R.id.button4);

        btnPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCam();
            }
        });

        //text to speech init

        mButtonSpeak = findViewById(R.id.button_speak);
        //Spinner part
        final Spinner mSpinner = (Spinner) findViewById(R.id.lang_spinner);
        //mAdapter
        ArrayAdapter<String> mAdapter = new ArrayAdapter<String>(MainActivity.this,
                android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.Languages));
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(mAdapter);

        mTTs = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                textToSpeechInit(status, mSpinner, mTTs);
            }
        });

        mEditText = findViewById(R.id.edit_text);
        mSeekBarPitch = findViewById(R.id.seek_bar_pitch);
        mSeekBarSpeed = findViewById(R.id.seek_bar_speed);

        mButtonSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                speak(mSpinner.getSelectedItem().toString());
            }
        });
    }

    public void openCam() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                imageView.setImageURI(resultUri);
                BitmapDrawable bitmapDrawable = (BitmapDrawable) imageView.getDrawable();
                bitmap = bitmapDrawable.getBitmap();
                isTrue = true;
                TextRecognizer textRecognizer = new TextRecognizer.Builder(getApplicationContext()).build();
                if (!textRecognizer.isOperational()) {
                    Log.e("ERROR", "DETECTOR DEPENDENCY IS NOT AVAILABLE");
                } else {
                    StringBuilder stringBuilder = new StringBuilder();
                    Frame frame = new Frame.Builder().setBitmap(bitmap).build();
                    SparseArray<TextBlock> items = textRecognizer.detect(frame);
                    for (int i = 0; i < items.size(); i++) {
                        TextBlock item = items.valueAt(i);
                        stringBuilder.append(item.getValue());
                        stringBuilder.append("\n");
                    }
                    s = stringBuilder.toString();
                    for (int i=0;i<10;i++) {
                        for (int j=0;j<10;j++) {
                            s = s.replace(letters[i][j],coreLetters[i]);
                        }
                    }
                    mEditText.setText(s);
                }
                finishActivity(requestCode);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
        finishActivity(requestCode);
    }

    public void speak(String language) {
        text = mEditText.getText().toString();

        //numbers to text, read trough string and replace with separate numbers

        if (language.equals("Lat")) {
            text = exchange(text, wordDigits);
        }

        float pitch = (float) mSeekBarPitch.getProgress() / 50;
        if (pitch < 0.1) pitch = 0.1f;
        float speed = (float) mSeekBarSpeed.getProgress() / 50;
        if (speed < 0.1) speed = 0.1f;

        mTTs.setLanguage(new Locale(language));
        mTTs.setPitch(pitch);
        mTTs.setSpeechRate(speed);


        mTTs.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    protected void onDestroy() {
        if (mTTs != null) {
            mTTs.stop();
            mTTs.shutdown();
        }
        System.out.println("On destroyed");
        super.onDestroy();
        isDestroyed = true;
    }

}
